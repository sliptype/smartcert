export const display = `
  font-family: 'Abril Fatface', cursive;
  font-size: 4rem;
`;

export const serif = `
  font-family: 'Roboto Slab', serif;
  line-height: 1.5rem;
  font-size: 1.1rem;
`;

export const light = `
  font-family: sans-serif;
  font-size: 1.2rem;
  font-weight: 100;
  font-feature-settings: "liga" 0;
  letter-spacing: .1rem;
`;


export const basic = `
  font-size: 1rem;
`;