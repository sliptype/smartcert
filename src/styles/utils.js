export const fixedWidth = (width) => `
  width: ${width};
  max-width: 100%;
  margin: auto;
`;