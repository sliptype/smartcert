export const primary = 'rgba(102, 153, 255, 1)';
export const accent = 'rgba(0, 98, 255, 1)';
export const secondary = 'white';
export const muted = 'gray';