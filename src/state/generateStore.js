import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware as createRouterMiddleware } from 'react-router-redux';

import createSagaMiddleware from 'redux-saga';
import rootReducerFactory from 'state/rootReducer';
import rootSagaFactory from 'state/rootSaga';

import createHistory from 'history/createBrowserHistory';

import { Drizzle, generateContractsInitialState } from 'drizzle';
import drizzleOptions from 'state/drizzleOptions';

import smartCertNormalizerFactory from 'state/contractNormalizer/smartCertNormalizer';

function generateStore() {

  const history = createHistory();
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const initialState = {
    contracts: generateContractsInitialState(drizzleOptions)
  };

  const smartCertNormalizer = smartCertNormalizerFactory();
  const routerMiddleware = createRouterMiddleware(history);
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(
    rootReducerFactory({
      normalized: smartCertNormalizer.reducer
    }),
    initialState,
    composeEnhancers(
      applyMiddleware(
        routerMiddleware,
        sagaMiddleware
      )
    )
  );

  const drizzle = new Drizzle(drizzleOptions, store);

  sagaMiddleware.run(rootSagaFactory([smartCertNormalizer.saga]));
  sagaMiddleware.setContext({
    drizzle
  });

  return { store, history, drizzle };
}

export default generateStore;
