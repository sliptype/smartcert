export const MENU_PANEL_OPEN = 'MENU_PANEL_OPEN';

export function menuPanelOpen(menuId) {
  return {
    type: MENU_PANEL_OPEN,
    payload: {
      menuId
    }
  };
};

export const MENU_PANEL_CLOSE = 'MENU_PANEL_CLOSE';

export function menuPanelClose(menuId) {
  return {
    type: MENU_PANEL_CLOSE,
    payload: {
      menuId
    }
  };
};
