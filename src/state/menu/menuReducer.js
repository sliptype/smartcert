import { MENU_PANEL_OPEN, MENU_PANEL_CLOSE } from 'state/menu/menuActions';

const initialState = {};

const closeAllMenus = (state) => {
  const result = {};

  for (let menu in state) {
    result[menu] = {
      ...state[menu],
      open: false
    };
  }

  return result;
}

const menuReducer = (state = initialState, action) => {
  switch (action.type) {

    case MENU_PANEL_OPEN: {
      let { payload: { menuId } } = action;

      return {
        ...closeAllMenus(state),
        [menuId]: {
          open: true
        }
      }
    }

    case MENU_PANEL_CLOSE: {
      let { payload: { menuId } } = action;
      return {
        ...state,
        [menuId]: {
          open: false
        }
      }
    }

    default: return state;
  }
};

export default menuReducer;
