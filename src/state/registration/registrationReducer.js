import { REGISTRATION_STATUS_RECEIVED } from 'state/registration/registrationActions';

const initialState = {
  registered: false,
  holder: false,
  issuer: false,
  checked: false
};

const registrationReducer = (state = initialState, action) => {
  switch (action.type) {

    case REGISTRATION_STATUS_RECEIVED:
      return {
        ...state,
        registered: action.payload[0] || action.payload[1],
        issuer: action.payload[0],
        holder: action.payload[1],
        checked: true
      };

    default: return state;
  }
};

export default registrationReducer;
