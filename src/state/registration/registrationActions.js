export const REGISTRATION_SUBMITTING = 'REGISTRATION_SUBMITTING';

export function registrationSubmit(details) {
  return {
    type: REGISTRATION_SUBMITTING,
    payload: details
  };
};

export const REGISTRATION_STATUS_RECEIVED = 'REGISTRATION_STATUS_RECEIVED';

export function registrationStatusReceive(details) {
  return {
    type: REGISTRATION_STATUS_RECEIVED,
    payload: details
  };
};

export const REGISTRATION_COMPLETE = 'REGISTRATION_COMPLETE';

export function registrationComplete() {
  return {
    type: REGISTRATION_COMPLETE
  };
};
