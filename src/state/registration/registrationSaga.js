import { take, put, getContext, fork, select } from 'redux-saga/effects';

import { takeEveryContractVar } from 'state/contractEffects';
import { registrationStatusReceive, registrationComplete } from 'state/registration/registrationActions';

function* watchSubmission() {
  let action = yield take('REGISTRATION_SUBMITTING');
  yield register(action);
}

function* watchStatus() {
  yield takeEveryContractVar('isRegistered', receiveStatus);
}

function* receiveStatus(status) {
  let drizzle = yield getContext('drizzle');
  let { issuer, holder } = yield select(state => state.registration);
  let account = yield select(state => state.accounts[0]);

  yield put(registrationStatusReceive(status));

  if (issuer || holder) {
    if (issuer) {
      drizzle.contracts.SmartCert.methods.issuers.cacheCall(account);
    } else if (holder) {
      drizzle.contracts.SmartCert.methods.holders.cacheCall(account);
    }
    // console.log('registrationComplete');
    // yield put(registrationComplete());
  }
}

function* register(action) {
  let drizzle = yield getContext('drizzle');

  switch (action.payload.type) {
    case 'issuer':
      drizzle.contracts.SmartCert.methods.registerIssuer.cacheSend(action.payload.name);
      break;
    case 'holder':
      drizzle.contracts.SmartCert.methods.registerHolder.cacheSend(action.payload.name, action.payload.email);
      break;
    default:
      break;
  }
}

function* registrationSaga() {
  yield fork(watchSubmission);
  yield fork(watchStatus);
};

export default registrationSaga;
