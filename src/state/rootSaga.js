import { all, fork } from 'redux-saga/effects';
import { drizzleSagas } from 'drizzle';
import registrationSaga from 'state/registration/registrationSaga';
import formSaga from 'state/form/formSaga';

const rootSagaFactory = (sagas) => function* root() {
  yield all(
    [
      fork(registrationSaga),
      fork(formSaga),
      ...drizzleSagas.map(saga => fork(saga)),
      ...sagas.map(saga => fork(saga))
    ]
  )
};

export default rootSagaFactory;