import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { drizzleReducers } from 'drizzle';
import { reducer as formReducer } from 'redux-form';
import registrationReducer from 'state/registration/registrationReducer';
import menuReducer from 'state/menu/menuReducer';
import { default as formTxReducer } from 'state/form/formReducer';

const rootReducerFactory = (reducers) => combineReducers({
  router: routerReducer,
  form: formReducer,
  formTx: formTxReducer,
  registration: registrationReducer,
  menu: menuReducer,
  ...drizzleReducers,
  ...reducers,
});

export default rootReducerFactory;
