import { take, takeEvery, put, getContext, fork, select } from 'redux-saga/effects';
import { FORM_SUBMIT, formSuccess, txStackIdReceive } from 'state/form/formActions';

function* watchSubmit() {
  yield takeEvery(FORM_SUBMIT, submitForm);
}

function* watchResult(options) {
  let { formId, transactionStackId, data, updateContractData, waitFor, onSuccess } = options;

  // TODO: handle other transactions states (errors)
  let { type, txHash } = yield take(action => {
    return action.type === 'TX_SUCCESSFUL';
  });

  let transactionId = yield select(state => state.transactionStack[transactionStackId]);

  if (transactionId === txHash) {
    if (updateContractData) {
      let drizzle = yield getContext('drizzle');
      updateContractData(drizzle, data);
    }

    if (waitFor) {
      yield waitFor();
    }

    yield put(formSuccess(formId));
    if (onSuccess) {
      yield put(onSuccess(data));
    }
  }
}

function* submitForm(action) {
  let { payload } = action;
  let { formId, method, data } = payload;

  let drizzle = yield getContext('drizzle');

  let transactionStackId = drizzle.contracts.SmartCert.methods[method].cacheSend(...data);
  yield put(txStackIdReceive(formId, transactionStackId));

  yield watchResult({ transactionStackId, ...payload });
}

function* formSaga() {
  yield fork(watchSubmit);
};

export default formSaga;
