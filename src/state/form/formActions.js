export const FORM_SUBMIT = 'FORM_SUBMIT';

export function formSubmit(payload) {
  return {
    type: FORM_SUBMIT,
    payload
  };
};

export const FORM_SUCCESS = 'FORM_SUCCESS';

export function formSuccess(formId, onSuccess) {
  return {
    type: FORM_SUCCESS,
    payload: {
      formId,
      onSuccess
    }
  };
};

export const TX_STACK_ID_RECEIVED = 'TX_STACK_ID_RECEIVED';

export function txStackIdReceive(formId, txStackId) {
  return {
    type: TX_STACK_ID_RECEIVED,
    payload: {
      formId,
      txStackId
    }
  };
};
