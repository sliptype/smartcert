import { FORM_SUBMIT, FORM_SUCCESS, TX_STACK_ID_RECEIVED } from 'state/form/formActions';

const initialState = {};

const formReducer = (state = initialState, action) => {
  switch (action.type) {

    case FORM_SUBMIT: {
      let { payload: { formId } } = action;
      return {
        ...state,
        [formId]: {
          ...state[formId],
          loading: true
        }
      }
    }

    case FORM_SUCCESS: {
      let { payload: { formId } } = action;
      return {
        ...state,
        [formId]: {
          ...state[formId],
          loading: false
        }
      }
    }

    case TX_STACK_ID_RECEIVED: {
      let { payload: { formId, txStackId } } = action;
      return {
        ...state,
        [formId]: {
          ...state[formId],
          stackId: txStackId
        }
      };

    }


    default: return state;
  }
};

export default formReducer;
