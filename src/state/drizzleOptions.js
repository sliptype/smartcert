import SmartCert from 'contracts/SmartCert.json';

const options = {
  contracts: [
    SmartCert
  ],
  events: {},
  polls: {},
  web3: {
    block: false,
    fallback: null
  }
};

export default options;
