import { take, takeEvery } from 'redux-saga/effects';

export function* takeContractVar(variable) {
  const { value } = yield take(action => action.type === 'GOT_CONTRACT_VAR' && action.variable === variable );
  return value;
};

export function* takeEveryContractVar(variable, method) {
  const filter = action => {
    return action.type === 'GOT_CONTRACT_VAR' && action.variable === variable
  };
  const saga = action => method(action.value);
  yield takeEvery(filter, saga);
};
