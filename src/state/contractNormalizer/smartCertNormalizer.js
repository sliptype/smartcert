import normalizer from  'state/contractNormalizer/contractNormalizer';

const smartCertNormalizerFactory = () => normalizer({
  entities: {
    issuers: {
      keys: ['id'],
      onReceive: (action, drizzle) => {
        drizzle.contracts.SmartCert.methods.getCertDefinitionIds.cacheCall(action.value.id);
      }
    },
    holders: {
      keys: ['id'],
      onReceive: (action, drizzle) => {
        drizzle.contracts.SmartCert.methods.getCertIds.cacheCall(action.value.id);
      }
    },
    certDefs: {
      keys: ['id', 'issuer'],
      onReceive: (action, drizzle) => {
        // drizzle.contracts.SmartCert.methods.issuers.cacheCall(action.value.issuer);
      }
    },
    certs: {
      keys: ['id', 'holder'],
      onReceive: (action, drizzle) => {
        drizzle.contracts.SmartCert.methods.certDefs.cacheCall(action.value.certDefId);
      }
    },
    getCertIds: {
      onReceive: (action, drizzle) => {
        action.value.forEach(id => drizzle.contracts.SmartCert.methods.certs.cacheCall(id));
      }
    },
    getCertDefinitionIds: {
      onReceive: (action, drizzle) => {
        action.value.forEach(id => drizzle.contracts.SmartCert.methods.certDefs.cacheCall(id));
      }
    }
  },
  namespace: 'SMARTCERT',
  onLoad: (drizzle) => {
    drizzle.contracts.SmartCert.methods.isRegistered.cacheCall();
  }
});

export default smartCertNormalizerFactory;