import { take, takeEvery, put, getContext, fork } from 'redux-saga/effects';
import { combineReducers } from 'redux';

export const denormalize = (entity, key, index) => {
  const firstLevel = entity[key][index];
  let secondLevel = [];

  if (key === 'id') {
    return firstLevel;
  }

  if (firstLevel) {
    secondLevel = firstLevel.map(id => entity.id[id]);
  }

  return secondLevel;
};


// Mutates the state so that ids are mapped to data 
const idStateMutation = (state, action) => {
  const { payload: { value } } = action;

  return {
    ...state,
    [value.id]: value
  };
};


// Mutates the state by pushing to an array of ids for a given key
const fieldStateMutation = (state, action, key) => {
  const { payload: { value } } = action;

  let index = value[key];
  let id = value.id;

  // Verify that the data does not already exist
  // TODO: might still have to update
  if (state[index] && state[index].includes(id)) {
    return state;
  }

  return {
    ...state,
    [index]: [
      ...(state[index] || []),
      id
    ]
  };
}

const reducerFactory = (actionType, mutation) => (state = {}, action) => {
  switch (action.type) {
    case actionType:
      return mutation(state, action);
    default: return state;
  }
}

const normalizedReducerFactory = (actionType, key) => reducerFactory(actionType, (state, action) => key === 'id' ? idStateMutation(state, action) : fieldStateMutation(state, action, key));

const entityReducerFactory = (actionType, keys) => combineReducers(keys.reduce((reducers, key) => ({
  ...reducers,
  [key]: normalizedReducerFactory(actionType, key)
}), {}));

const createNormalizedAction = (type, payload) => ({
  type,
  payload
});

const actionTypeFactory = (namespace, entityName) => {
  // const actionTypes = ['RECEIVED'];
  // return actionTypes.map(action => `${namespace}_${entityName}_${action}`);
  return `${namespace}_${entityName}_RECEIVED`.toUpperCase();
}


/*
options = {
    entities: {
      issuers: {
        keys: ['id'],
        onReceive: (action, drizzle) => {
          // console.log(action, drizzle);
          drizzle.contracts.SmartCert.methods.getCertDefinitionIds.cacheCall(action.value.id);
        }
      },
      holders: {
        keys: ['id'],
        onReceive: (action, drizzle) => {

        }
      },
      certDefs: {
        keys: ['id', 'issuer'],
        onReceive: (action, drizzle) => {
          drizzle.contracts.SmartCert.methods.issuers.cacheCall(action.value.issuer);
        }
      },
      certs: {
        keys: ['id', 'holder'],
        onReceive: (action, drizzle) => {

        }
      },
    },
    namespace: 'SMARTCERT'
  });
*/

const entitySagaFactory = (actionTypes, callbacks, onLoad) => {

  const entities = Object.keys(actionTypes);

  const receiveSaga = function* (action) {
    let drizzle = yield getContext('drizzle');

    if (callbacks[action.variable]) {
      callbacks[action.variable](action, drizzle);
    }

    yield put(createNormalizedAction(actionTypes[action.variable], action));
  }

  const watchInit = function* () {
    yield take('DRIZZLE_INITIALIZED');
    let drizzle = yield getContext('drizzle');
    onLoad(drizzle);
  }

  const watchSaga = function* () {
    const filter = action => {
      return action.type === 'GOT_CONTRACT_VAR' && entities.includes(action.variable);
    };

    yield takeEvery(filter, receiveSaga);
  };

  return function* saga() {
    yield fork(watchInit);
    yield fork(watchSaga);
  };
}

const ContractNormalizer = (options) => {

  const { entities, namespace, onLoad } = options;
  const actionTypes = {};
  const callbacks = {};
  const reducers = {};

  for (let entityId in entities) {
    const { keys, onReceive } = entities[entityId];

    // map entity to action type
    actionTypes[entityId] = actionTypeFactory(namespace, entityId);

    callbacks[entityId] = onReceive; 

    if (keys) {
      // map entity to reducer ( composed of key reducers )
      reducers[entityId] = entityReducerFactory(actionTypes[entityId], keys);
    }
  }

  return {
    saga: entitySagaFactory(actionTypes, callbacks, onLoad),
    reducer: combineReducers(reducers)
  };
}


export default ContractNormalizer;