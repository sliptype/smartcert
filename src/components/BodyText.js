import { serif } from 'styles/fonts.js';
import styled from 'styled-components'

const BodyText = styled.div`
  ${ serif }
`;

export default BodyText;