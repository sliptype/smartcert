import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import styled from 'styled-components';

import RegistrationCard from 'components/RegistrationCard';
import HeadingText from 'components/HeadingText';
import BodyText from 'components/BodyText';

const RegistrationCardWrapper = styled.div`
  margin: 3rem;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 3rem;
`;

const Welcome = props => {
  return (
    <div>
      <HeadingText>Welcome!</HeadingText>
      <BodyText>To get started, register as one of the following:</BodyText>
      <RegistrationCardWrapper>
         <RegistrationCard
          key='holder'
          title='Certification Holder'
          description='Receive, display, and prove ownership of your certifications'
          action={ () => props.registrationStart('holder') }
          />
        <RegistrationCard
          key='issuer'
          title='Certification Issuer'
          description='Define and issue certifications'
          action={ () => props.registrationStart('issuer') }
          />
      </RegistrationCardWrapper>
    </div>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    registrationStart: type => {
      dispatch(push(`/register/${type}`));
    }
  }
}


export default connect(null, mapDispatchToProps)(Welcome);
