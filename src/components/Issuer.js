import React, { Component } from 'react';
import { drizzleConnect } from 'drizzle-react';
import { push } from 'react-router-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import HeadingText from 'components/HeadingText';
import Button from 'components/Button';
import Loader from 'components/Loader';
import Table from 'components/Table';
import Inset from 'components/Inset';

import { denormalize } from 'state/contractNormalizer/contractNormalizer';

import { light } from 'styles/fonts';
import { primary, muted } from 'styles/colors';

const HeadingGrid = styled.div`
  display: grid;
  grid-template-columns: auto 1fr auto;
  align-items: baseline;
  justify-content: start;
  grid-column-gap: 1rem;
`;

const TableFooter = styled.div`
  display: grid;
  grid-template-columns: 1fr auto;
  justify-content: start;
`;

const TotalDisplay = styled.div`
  font-size: 1.5rem;
  font-weight: 600;
  color: ${ primary };
  margin-right: 2px;
`;

const ShortenedAddress = styled.div`
  max-width: 10rem;
  text-overflow: ellipsis;
  overflow: hidden;

  ${ light }
  color: ${ muted };
`;

const buildTableData = (certDefs) => 
  certDefs.map(certDef => ({
    type: certDef.name,
    quantity: certDef.quantity
  }));

const issueTotal = (certDefs) => certDefs.reduce((total, certDef) => total + Number.parseInt(certDef.quantity, 10), 0);

class Issuer extends Component {

  constructor(props, context) {
    super(props, context);
    this.issuerId = props.match.params.id || props.account;
    context.drizzle.contracts.SmartCert.methods.issuers.cacheCall(this.issuerId);
  }

  render() {
    const issuer = denormalize(this.props.issuers, 'id', this.issuerId);

    if (issuer) {
      const { name } = issuer;
      const isSelf = this.props.account === this.issuerId;
      let newCertButton;
      let newCertDefButton;

      if (isSelf) {
        newCertButton = <Button verticalCenter text='Issue New Cert' click={ this.props.issueCert }/>;
        newCertDefButton = <Button link text='+ New Cert Type' click={ this.props.addCertType }/>;
      }

      const certDefs = denormalize(this.props.certDefs, 'issuer', this.issuerId);
      let tableData;
      if (certDefs.length) {
        tableData = buildTableData(certDefs);
      } else {
        tableData = [{
          type: 'Add a new certification type to get started!',
          quantity: '0'
        }];
      }

      return (
        <div>
          <HeadingGrid>
            <HeadingText>{ name }</HeadingText>
            <div>
              <ShortenedAddress>{ this.issuerId }</ShortenedAddress>
            </div>
            { newCertButton }
          </HeadingGrid>
          <Inset>
            <Table data={ tableData } columns={[
              {
                name: 'Certification Type',
                accessor: 'type'
              },
              {
                name: 'Number of holders',
                accessor: 'quantity',
                align: 'right'
              }
            ]} />
            <TableFooter>
              <div>
              { newCertDefButton }
              </div>
              <TotalDisplay>
                Total: { issueTotal(certDefs) }
              </TotalDisplay>
            </TableFooter>
          </Inset>
        </div>
      );

    }

    return (<Loader/>);
  }
}

Issuer.contextTypes = {
  drizzle: PropTypes.object
}

const mapStateToProps = state => ({
  account: state.accounts[0],
  issuers: state.normalized.issuers,
  certDefs: state.normalized.certDefs
});

const mapDispatchToProps = dispatch => ({
  issueCert: () => dispatch(push('/issue')),
  addCertType: () => dispatch(push('/define'))
});

export default drizzleConnect(Issuer, mapStateToProps, mapDispatchToProps);
