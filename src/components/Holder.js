import React, { Component } from 'react';
import { push } from 'react-router-redux';
import PropTypes from 'prop-types';

import { drizzleConnect } from 'drizzle-react'

import Table from 'components/Table';
import HeadingText from 'components/HeadingText';
import Loader from 'components/Loader';

import moment from 'moment';

import { denormalize } from 'state/contractNormalizer/contractNormalizer';

const buildTableData = (certs, certDefs, issuers, push) => 
  certs.filter(cert => denormalize(certDefs, 'id', cert.certDefId))
    .map(cert => {
    let certDef = denormalize(certDefs, 'id',  cert.certDefId);
    let issuer = denormalize(issuers, 'id', certDef.issuer);

    return {
      name: certDef.name,
      issuer: issuer ? issuer.name : 'Unknown',
      issuerClick: () => push(`/issuer/${issuer.id}`),
      date: moment.unix(cert.date).format('LL'),
    };
  });

class Holder extends Component {

  constructor(props, context) {
    super(props);
    this.holderId = props.match.params.id || props.account;
    context.drizzle.contracts.SmartCert.methods.holders.cacheCall(this.holderId);
  }

  render() {
    const holder = denormalize(this.props.holders, 'id', this.holderId);

    if (holder) {
      const { id, name } = holder;
      const isSelf = this.props.account === id;
      const certs = denormalize(this.props.certs, 'holder', id);
      let tableData;
      if (certs.length) {
        tableData = buildTableData(certs, this.props.certDefs, this.props.issuers, this.props.push);
      } else {
        tableData = [{
          name: 'No certifications yet'
        }];
      }
      return (
        <div>
          <HeadingText>{ isSelf ? 'My Certifications' : `${ name }'s Certifications` }</HeadingText>
          <Table data={ tableData } columns={[
            {
              name: 'Certification',
              accessor: 'name'
            },
            {
              name: 'Issuer',
              accessor: 'issuer',
              onClick: 'issuerClick'
            },
            {
              name: 'Date',
              accessor: 'date'
            }
          ]}
          />
        </div>
      );
    }

    return (<Loader/>);
  }
}

Holder.contextTypes = {
  drizzle: PropTypes.object
}

const mapStateToProps = state => ({
  account: state.accounts[0],
  holders: state.normalized.holders,
  issuers: state.normalized.issuers,
  certs: state.normalized.certs,
  certDefs: state.normalized.certDefs,
});

const mapDispatchToProps = dispatch => ({
  push: route => dispatch(push(route))
});

export default drizzleConnect(Holder, mapStateToProps, mapDispatchToProps);
