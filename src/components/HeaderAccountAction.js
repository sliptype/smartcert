import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { menuPanelOpen, menuPanelClose } from 'state/menu/menuActions';
import { primary, secondary } from 'styles/colors';

import HeaderAction from 'components/HeaderAction';
import MaterialIcon from 'components/MaterialIcon';

const ActionPanel = styled.div`
  position: absolute;
  max-width: 300px;
  top: 50px;
  right: -5px;
  background-color: ${ secondary };
  border: 5px solid ${ primary };
  border-top: none;
  padding: 3rem;
  text-align: center;
  z-index: 3;
`;

const StyledIcon = styled(MaterialIcon)`
  font-size: 5rem !important;
  color: ${ primary };
  margin: 1rem;
`;

const LinkContainer = styled.input`
  padding: 1rem;
`;

class HeaderAccountAction extends Component {

  constructor(props) {
    super(props);

    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  /**
   * Set the wrapper ref
   */
  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside(event) {
    if (this.props.open && this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.props.closeMenu();
    }
  }

  render() {
    let panel;

    if (this.props.open) {
      panel = (
        <ActionPanel>
          <StyledIcon icon='how_to_reg'/>
          <p>{ this.props.holder ? 'Show off your certifications!' : 'Show off your stats!' }</p>
          <p>Share this link:</p>
          <LinkContainer value={ this.props.shareLink } disabled />
        </ActionPanel>
      );
    }

    return (
      <div ref={ this.setWrapperRef }>
        <div onClick={ this.props.open ? this.props.closeMenu : this.props.openMenu }>
          <HeaderAction icon='account_box' invert={ this.props.open }></HeaderAction>
        </div>
        { panel }
      </div>
    );

  }
}

const mapStateToProps = (state, props) => {
  let shareLink;

  if (state.registration.holder) {
    shareLink = `${ window.location.host }/holder/${ state.accounts[0] }`;
  } else {
    shareLink = `${ window.location.host }/issuer/${ state.accounts[0] }`;
  }

  return {
    open: state.menu[props.id] && state.menu[props.id].open,
    shareLink
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  openMenu: () => dispatch(menuPanelOpen(props.id)),
  closeMenu: () => dispatch(menuPanelClose(props.id))
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderAccountAction);