import React from 'react';
import styled from 'styled-components'
import { Field } from 'redux-form';
import { basic } from 'styles/fonts';

const StyledLabel = styled.label`
  display: block;
  margin-top: 1rem;
`;

const StyledField = styled(Field)`
  width: 100%;
  max-width: 100%;
  padding: .5rem;
  ${ basic };
  -webkit-appearance: none;
  border-radius: 0;
  background-color: white;
  border: 1px solid grey;
`;

const FormElement = (props) => {
  const name = props.name.toLowerCase();
  let optionElements;

  if (props.component === 'select') {
    optionElements = [<option key='none' value='' disabled defaultValue>Please select:</option>];
    optionElements = optionElements.concat(props.options.map((option) => (<option key={ option.name } value={ option.id }>{ option.name }</option>)));
  }

  const { options, ...propsToPass } = props;

  return (
    <div>
      <StyledLabel htmlFor={ name }>{ props.label || props.name }</StyledLabel>
      <StyledField { ...propsToPass } name={ name } required>
        { optionElements }
      </StyledField>
    </div>
  );
}

export default FormElement;
