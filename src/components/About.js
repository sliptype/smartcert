import React from 'react';
import styled from 'styled-components';

import HeadingText from 'components/HeadingText';
import BodyText from 'components/BodyText';

import { light } from 'styles/fonts';

const Heading = styled.div`
  font-size: 1.2rem;
  margin-top: 2rem;
`;

const About = props => (
  <div>
    <HeadingText>
      About
    </HeadingText>
    <BodyText>
      SmartCert is a proof of concept built on the Ethereum blockchain. It allows companies to more easily issue certifications and individuals to verifiably prove that they have received certifications.
    </BodyText>

    <Heading>
      How do I get started?
    </Heading>
    <BodyText>
      To get started, you will need a way to connect to the Ethereum blockchain. Popular options include MetaMask or Parity. Currently, SmartCert is only deployed on the Ropsten testnet. After connecting to Ropsten using your browser of choice, SmartCert will take you through a registration process.
    </BodyText>

    <Heading>
      How do I prove I own an account?
    </Heading>
    <BodyText>
      If you own an Ethereum address, you can sign a message with your private key and anyone with access to your public key can decrypt the message. If the message is decrypted correctly, that means you own the account!
    </BodyText>
    <BodyText>
      You can sign and verify messages on&nbsp;
      <a href='https://www.myetherwallet.com/signmsg.html'>
        MyEtherWallet
      </a>
    </BodyText>

    <Heading>
      How do I contribute?
    </Heading>
    <BodyText>
      SmartCert is built using Solidity, React, Redux, and Drizzle. You can find the repo&nbsp;
      <a href='https://gitlab.com/sklingler93/smartcert'>here</a>.
      Any and all help is appreciated!
    </BodyText>
  </div>
);

export default About;
