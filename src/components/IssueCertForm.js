import React, { Component } from 'react';
import { connect } from 'react-redux'
import uuid from 'uuid/v1';

import ContractForm from 'components/ContractForm';
import HeadingText from 'components/HeadingText';
import BodyText from 'components/BodyText';

import { denormalize } from 'state/contractNormalizer/contractNormalizer';
import { push } from 'react-router-redux';
import { takeContractVar } from 'state/contractEffects';

class IssueCertForm extends Component {
  constructor(props, context) {
    super(props);
    this.formId = uuid();
  }

  render() {
    return (   
      <div>
        <HeadingText>Issue</HeadingText>
        <BodyText>Issue a new certification by providing the following info:</BodyText>
        <ContractForm inputs={ this.props.inputs } action={ this.props.action } formId={ this.formId }/>
      </div>
    );
  }
};

const mapStateToProps = state => ({
  inputs: [
    {
      name: 'holder',
      label: 'Recipient',
      component: 'input',
      type: 'text',
      placeholder: '0x0000...'
    },
    {
      name: 'definition',
      label: 'Certification Type',
      component: 'select',
      options: denormalize(state.normalized.certDefs, 'issuer', state.accounts[0])
    }
  ],
  action: {
    label: 'Issue',
    method: 'issueCert',
    updateContractData: (drizzle, args) => {
      drizzle.contracts.SmartCert.methods.certs.cacheCall(args[0]);
    },
    waitFor: function* () {
      yield takeContractVar('certs');
    },
    onSuccess: (data) => push(`/holder/${data[0]}`)
  }
});

export default connect(mapStateToProps, null)(IssueCertForm);
