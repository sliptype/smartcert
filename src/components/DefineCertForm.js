import React, { Component } from 'react';
import { connect } from 'react-redux';
import uuid from 'uuid/v1';
import { push } from 'react-router-redux';
import { takeContractVar } from 'state/contractEffects';

import ContractForm from 'components/ContractForm';
import HeadingText from 'components/HeadingText';
import BodyText from 'components/BodyText';

class DefineCertForm extends Component {
  constructor(props, context) {
    super(props);
    this.formId = uuid();

    this.inputs = [
      {
        name: 'name',
        label: 'Name',
        component: 'input',
        type: 'text',
        placeholder: 'Certification'
      },
      {
        name: 'description',
        label: 'Description',
        component: 'textarea'
      }
    ];

    this.action = {
      label: 'Save',
      method: 'addDefinition',
      updateContractData: (drizzle, args) => {
        drizzle.contracts.SmartCert.methods.getCertDefinitionIds.cacheCall(props.account);
      },
      waitFor: function* () {
        yield takeContractVar('certDefs');
      },
      onSuccess: () => push('/')
    };
  }

  render() {
    return (   
      <div>
        <HeadingText>Define</HeadingText>
        <BodyText>Define a new certification type by providing the following info:</BodyText>
        <ContractForm inputs={ this.inputs } action={ this.action } formId={ this.formId }/>
      </div>
    );
  }
};

const mapStateToProps = state => ({
  account: state.accounts[0]
});

export default connect(mapStateToProps, null)(DefineCertForm);
