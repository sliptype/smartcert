import React from 'react';

const MaterialIcon = (props) => (
  <i className={`material-icons ${props.className}`}>{ props.icon }</i>
);

export default MaterialIcon;
