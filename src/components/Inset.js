import styled from 'styled-components';
import { fixedWidth } from 'styles/utils';

const Inset = styled.div`
  ${ fixedWidth('90%') }
`;

export default Inset;
