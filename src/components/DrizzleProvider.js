import { Component, Children } from 'react'
import PropTypes from 'prop-types'
import { Drizzle, generateStore } from 'drizzle'


class DrizzleProvider extends Component {
  static propTypes = {
    options: PropTypes.object,
    drizzle: PropTypes.object,
    store: PropTypes.object
  }

  // you must specify what you’re adding to the context
  static childContextTypes = {
    drizzle: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }

  constructor(context, props) {
    super(context, props)
  }

  getChildContext() {
    const store = this.props.store ? this.props.store : generateStore(this.props.options)

    // TODO: Make a PR to drizzle-react with the following change:
    const drizzle = this.props.drizzle || new Drizzle(this.props.options, store)

    return { drizzle, store };
  }

  render() {
    return Children.only(this.props.children)
  }
}

export default DrizzleProvider
