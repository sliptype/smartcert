import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import styled from 'styled-components';

import FormElement from 'components/FormElement';
import Button from 'components/Button';
import Loader from 'components/Loader';

import { connect } from 'react-redux';
import { formSubmit } from 'state/form/formActions';

const StyledForm = styled.form`
  position: relative;
  width: 100%;
  max-width: 400px;  
  margin: 2rem auto;
`;

const LoadingOverlay = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 2;
  background-color: rgba(255, 255, 255, .5);
`;

const ActionContainer = styled.div`
  width: 100%;
  text-align: right;
`;

const Form = id => reduxForm({ form: id })(props => {

  let loadingOverlay;

  if (props.loading) {
    loadingOverlay = (
      <LoadingOverlay>
        <Loader />
      </LoadingOverlay>
    );
  }

  return (
    <div>
      <StyledForm onSubmit={ props.handleSubmit }>
        { loadingOverlay }
        {
          props.inputs.map((input, index) => {
            return (<FormElement key={ index } { ...input }/>);
          })
        }
        <ActionContainer>
          <Button type='submit' text={ props.action.label } loading={ props.loading }/>
        </ActionContainer>
      </StyledForm>
    </div>
  )
});

class ContractForm extends Component {

  constructor(props, context) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.FormWithId = Form(this.props.formId);
  }

  handleSubmit(data) {
    let inputsWithIndex = this.props.inputs.map((input, i) => input.argIndex >= 0 ? input : { ...input, argIndex: i });
    let sortedInputs = inputsWithIndex.sort((a, b) => a.argIndex > b.argIndex ? 1 : -1);
    let sortedData = sortedInputs.map(input => data[input.name]);

    this.props.onSubmit({
      formId: this.props.formId,
      method: this.props.action.method,
      data: sortedData,
      updateContractData: this.props.action.updateContractData,
      waitFor: this.props.action.waitFor,
      onSuccess: this.props.action.onSuccess
    });
  }

  render() {
    return <this.FormWithId { ...this.props } onSubmit={ this.handleSubmit }/>
  }
}

const mapStateToProps = (state, props) => ({
  loading: state.formTx[props.formId]
});

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (args) => dispatch(formSubmit(args))
});

export default connect(mapStateToProps, mapDispatchToProps)(ContractForm);
