import React from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router';

import { Route } from 'react-router';
import { connect } from 'react-redux';
import LoaderContainer from 'components/LoaderContainer';
import Welcome from 'components/Welcome';
import RegistrationForm from 'components/RegistrationForm';
import Issuer from 'components/Issuer';
import Holder from 'components/Holder';
import IssueCertForm from 'components/IssueCertForm';
import DefineCertForm from 'components/DefineCertForm';
import About from 'components/About';

import { fixedWidth } from 'styles/utils.js';

const ContentWrapper = styled.main`
  ${ fixedWidth('900px') }
  padding: 0 1rem;
  margin-top: 50px;
  margin-bottom: 100px;
`;

const Content = props => {

  let entryComponent = Welcome;

  if (props.registration.registered) {
    entryComponent = props.registration.issuer ? Issuer : Holder;
  }

  return (<ContentWrapper>
      <LoaderContainer>
        <div>
          <Route path='/' exact component={ entryComponent } />
          <Route path='/welcome' component={ Welcome } />
          <Route path='/register/:type(holder|issuer)' component={ RegistrationForm } />
          <Route path='/issuer/:id' component={ Issuer } />
          <Route path='/holder/:id' component={ Holder } />
          <Route path='/issue' component={ IssueCertForm } />
          <Route path='/define' component={ DefineCertForm } />
          <Route path='/about' component={ About } />
        </div>
      </LoaderContainer>
    </ContentWrapper>
  );

}

const mapStateToProps = state => ({
  registration: state.registration,
  location: state.router.location
});

export default withRouter(connect(mapStateToProps, null)(Content));
