import React, { Component } from 'react';
import styled from 'styled-components'

import { light } from 'styles/fonts';
import { primary } from 'styles/colors';

const StyledTable = styled.table`
  width: 100%;
  margin: 2rem auto 0;
`;

class Table extends Component {

  constructor(props, context) {
    super(props);
  }

  buildHeader(index, col) {
    const StyledHeader = styled.th`
      text-align: left;
      ${ light }
      ${ col.align === 'right' ? 'text-align: right' : '' }
    `;
    return (<StyledHeader key={ index }> { col.name } </StyledHeader>);
  }

  buildCell(index, data, col) {
    const StyledCell = styled.td`
      font-size: 1.5rem;
      padding: 1rem 0;
      ${ col.align === 'right' ? 'text-align: right' : '' }
    `;

    if (data[col.onClick]) {

      const ClickableCell = styled(StyledCell)`
        // color: ${ primary };
        cursor: pointer;
      `;
      return (<ClickableCell key={ index } onClick={ data[col.onClick] }>{ data[col.accessor] }</ClickableCell>);
    }
    return (<StyledCell key={ index } >{ data[col.accessor] }</StyledCell>);
  }

  buildHeaders(columns) {
    return columns.map((col, i) => this.buildHeader(i, col));
  }

  buildRow(columns, rowData) {
    return columns.map((col, i) => this.buildCell(i, rowData, col));
  }

  buildData(data, columns) {
    const StyledRow = styled.tr`
      height: 2rem;
    `;
    return data.map((row, i) => (<StyledRow key={ i }>{ this.buildRow(this.props.columns, row) }</StyledRow>));
  }

  render() {

    let headers = this.buildHeaders(this.props.columns);
    let data = this.buildData(this.props.data, this.props.columns);

    return (
      <StyledTable>
        <tbody>
          <tr>{ headers }</tr>
          { data }
        </tbody>
      </StyledTable>
    )
  }
}

export default Table;
