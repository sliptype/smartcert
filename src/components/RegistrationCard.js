import React, { Component } from 'react';
import styled from 'styled-components';

import Button from 'components/Button';

import { primary, secondary } from 'styles/colors.js';
import { light, serif } from 'styles/fonts.js';

const Card = styled.div`
  border: 2px solid ${ primary };
  text-align: center;
  display: grid;
  grid-template-rows: 3rem 9rem;
`;

const Header = styled.div`
  ${ light }
  height: 100%;
  background-color: ${ primary };
  color: ${ secondary };
  display: grid;
  align-items: center;
`;

const Body = styled.div`
  height: 100%;
  padding: 0 1rem;
  display: grid;
  grid-template-rows: 5rem 3rem;
  align-items: center;
  ${ serif };
`;

export default class Content extends Component {
  render() {
    return (
      <Card>
        <Header>
          <div>{ this.props.title }</div>
        </Header>
        <Body>
          <div>{ this.props.description }</div>
          <Button text='Register' click={ this.props.action }/>
        </Body>
      </Card>
    );
  }
}
