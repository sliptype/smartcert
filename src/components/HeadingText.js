import { display } from 'styles/fonts.js';
import styled from 'styled-components'

const HeadingText = styled.div`
  ${ display }
`;

export default HeadingText;
