import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import uuid from 'uuid/v1';

import ContractForm from 'components/ContractForm';
import HeadingText from 'components/HeadingText';
import BodyText from 'components/BodyText';
import Button from 'components/Button';

import { REGISTRATION_STATUS_RECEIVED } from 'state/registration/registrationActions';
import { take } from 'redux-saga/effects';

class RegistrationForm extends Component {
  constructor(props, context) {
    super(props);
    this.formId = uuid();
  }

  render() {
    const { registered, holder } = this.props.registration;

    if (registered) {
      return (
        <div>
          <HeadingText>Register</HeadingText>
          <BodyText>You are already registered as a certification { holder ? 'holder' : 'issuer' }</BodyText>
          <Button click={ this.props.returnHome } text='Return Home' align='right'/>
        </div>
      )
    }

    return (   
      <div>
        <HeadingText>Register</HeadingText>
        <BodyText>Please provide the following info:</BodyText>
        <ContractForm inputs={ this.props.inputs } action={ this.props.action } formId={ this.formId }/>
      </div>
    );
  }
};

const mapStateToProps = (state, props) => {
  let inputs = [
    {
      name: 'name',
      label: 'Name',
      component: 'input',
      type: 'text'
    }
  ];

  let action = {
    label: 'Register',
    method: 'registerIssuer',
    waitFor: function* () {

      // TODO: Handle this in a better way
      // Emitting REGISTRATION_COMPLETE results in the welcome view flashing before the redirect happens
      yield take((action) => action.type === 'GOT_CONTRACT_VAR' && action.variable === 'isRegistered' && (action.value[0] || action.value[1]));
    },
    onSuccess: () => push('/')
  };

  if (props.match.params.type === 'holder') {
    inputs.push({
      name: 'email',
      label: 'Email',
      component: 'input',
      type: 'email'
    });

    action.method = 'registerHolder';
  }

  return {
    registration: state.registration,
    inputs,
    action
  };
};

const mapDispatchToProps = dispatch => ({
  returnHome: () => dispatch(push('/'))
});

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationForm);

