import React from 'react';
import { CubeGrid } from 'styled-spinkit';
import { primary } from 'styles/colors';

const Loader = () => (
  <main>
    <CubeGrid color={ primary }/>
  </main>
);

export default Loader;
