import React from 'react';
import styled from 'styled-components';

// import { connect } from 'react-redux';
// import { push } from 'react-router-redux';

import MaterialIcon from 'components/MaterialIcon';

import { primary, secondary } from 'styles/colors';


const HeaderAction = props => {
  const Icon = styled(MaterialIcon)`
    font-size: 2rem !important;
    color: ${ props.invert ? primary : secondary };
  `;

  const IconButton = styled.button`
    background-color: ${ props.invert ? secondary: primary };
    border: none;
    cursor: pointer;
    height: 50px;
    width: 50px;
  `;

  return (
    <IconButton>
      <Icon icon={ props.icon }/>
    </IconButton>
  );
};


// const mapDispatchToProps = dispatch => {
//   return {
//     routeReset: () => dispatch(push('/'))
//   }
// }

export default HeaderAction;