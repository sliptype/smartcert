import React from 'react';
import styled from 'styled-components';

import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import HeaderAccountAction from 'components/HeaderAccountAction';
import HeaderAction from 'components/HeaderAction';

import { fixedWidth } from 'styles/utils';
import { primary } from 'styles/colors';

const HeaderWrapper = styled.header`
  height: 50px;
  background-color: ${ primary };
`;

const Navigation = styled.nav`
  ${ fixedWidth('900px') }
  height: 100%;
  display: grid;
  grid-template-columns: auto 1fr;
  justify-content: start;
  align-items: center;
  position: relative;
`;

const Logo = styled.h1`
  color: white;
  cursor: pointer;
  font-size: 1.5rem;
  margin: 0;
`;

const Actions =  styled.div`
  justify-self: end;
  display: grid;
  grid-template-columns: repeat(2, auto);
`;

const Header = props => {

  let accountAction;

  if (props.registered) {
    accountAction = (<HeaderAccountAction id='accountMenu' />);
  }

  return (
    <HeaderWrapper>
      <Navigation>
        <Logo onClick={ props.routeReset }>SmartCert</Logo>
        <Actions>
          <div onClick={ props.help }>
            <HeaderAction icon='help_outline'/>
          </div>
          { accountAction }
        </Actions>
      </Navigation>
    </HeaderWrapper>
  );
};

const mapStateToProps = state => ({
  registered: state.registration.registered
});

const mapDispatchToProps = dispatch => {
  return {
    routeReset: () => dispatch(push('/')),
    help: () => dispatch(push('/about'))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);