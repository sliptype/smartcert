import React, { Children } from 'react';
import { drizzleConnect } from 'drizzle-react';
import styled from 'styled-components';

import Loader from 'components/Loader';
import BodyText from 'components/BodyText';
import MaterialIcon from 'components/MaterialIcon';
import { primary } from 'styles/colors';

const StyledError = styled.div`
  margin: auto;
  width: 500px;
  text-align: center;
`;

const StyledIcon = styled(MaterialIcon)`
  font-size: 5rem !important;
  color: ${ primary };
  margin: 1rem;
`;

function LoaderContainer(props) {

  if (props.hideError) {
    return Children.only(props.children);
  }

  if (props.web3Failed) {
    return(
      <StyledError>
        <StyledIcon icon='error_outline' />
        <div>
          <div>
            This browser has no connection to the Ethereum network.
          </div>
          <div>
            Please use the Chrome/FireFox extension MetaMask, or dedicated Ethereum browsers Mist or Parity.
          </div>
        </div>
      </StyledError>
    );
  }

  if (props.accountError) {
    return(
      <StyledError>
        <StyledIcon icon='error_outline' />
        <div>
          <div>
            Could not find any accounts.
          </div>
          <div>
            If using MetaMask, please log in
          </div>
        </div>
      </StyledError>
    );
  }

  if (!props.loaded) {
    return(<Loader/>);
  }

  return Children.only(props.children);

};

const mapStateToProps = state => {
  const drizzleInitialized = state.drizzleStatus.initialized;
  const accountsExist = state.accounts && state.accounts[0];

  return {
    web3Failed: state.web3.status === 'failed',
    accountError: drizzleInitialized && !accountsExist,
    loaded: state.registration.checked,
    hideError: state.router.location.pathname === '/about'
  };
};

export default drizzleConnect(LoaderContainer, mapStateToProps);
