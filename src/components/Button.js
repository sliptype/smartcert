import React from 'react';
import styled from 'styled-components';

import { primary, secondary, accent } from 'styles/colors.js';
import { light } from 'styles/fonts.js';

export default (props) => {

  const BaseButton = styled.button`
    ${ light }
    font-weight: 500;
    cursor: pointer;
    border: none;

    ${ props.align === 'right' ? 'float: right' : '' }

    ${ props.verticalCenter ? 'align-self: center' : '' }
  `;

  const ButtonStyled = styled(BaseButton)`
    text-align: center;
    min-width: 150px;
    padding: .5rem 2rem;
    margin: .5rem auto;
    background-color: ${ primary };
    color: ${ secondary };

    &:hover {
      background-color: ${ accent };
    }
  `;

  const LinkStyled = styled(BaseButton)`
    background-color: transparent;
    color: ${ primary };

    &:hover {
      color: ${ accent };
    }
  `;

  let content = props.text;

  if (props.loading) {
    content = 'Loading...';
  }

  if (props.link) {
    return (
      <LinkStyled onClick={ props.click } type={ props.type }>
        { content }
      </LinkStyled>
    );
  } else {
    return (
      <ButtonStyled onClick={ props.click } type={ props.type }>
        { content }
      </ButtonStyled>
    );
  }

};