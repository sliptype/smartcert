import React from 'react';
import { render } from 'react-dom';

import { ConnectedRouter } from 'react-router-redux';

import App from 'components/App';
import DrizzleProvider from 'components/DrizzleProvider';

import generateStore from 'state/generateStore';
import registerServiceWorker from 'registerServiceWorker';
import { menuPanelsClose } from 'state/menu/menuActions';

import 'normalize.css';
import 'index.css';

const { store, history, drizzle } = generateStore();
registerServiceWorker();

render(
  <DrizzleProvider store={ store } drizzle={ drizzle }>
    <ConnectedRouter history={ history }>
      <App/>
    </ConnectedRouter>
  </DrizzleProvider>,
  document.getElementById('root')
);

