# SmartCert

SmartCert is a decentralized app for managing and issuing certifications.

## About

SmartCert was initialized using [create-react-app](https://github.com/facebook/create-react-app).
It uses [drizzle](https://github.com/trufflesuite/drizzle) to manage blockchain related state.

## Develop

1. Start Ganache
2. Open Metamask and restore from seed phrase
3. Run `truffle migrate --reset`
2. Run `yarn move-contracts`. This will move the compiled contract abi's somewhere that the build can find them
4. Run `yarn start`
